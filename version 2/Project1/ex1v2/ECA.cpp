#include "ECA.h"


#include "RandomTools.h"
#include "BitTools.h"
#include <sstream>
#include <iomanip>
#include <cassert>


void initializeSpaceTime(SpaceTime & spaceTime)
{
	spaceTime.cells = nullptr;
	spaceTime.spaceSize = 0ull;
	spaceTime.timeSize = 0ull;
}

void allocSpaceTime(SpaceTime & spaceTime, size_t spaceSize, size_t timeSize)
{
	assert(spaceSize >= 3 && timeSize > 0); // le 3 garantie qu'on aura un espace viable pour la suite - ainsi, pas besoin de mettre des if partout pour valider le dépassement de l'espace lors des calculs.

	if (spaceTime.cells) {
		deallocSpaceTime(spaceTime);
	}

	spaceTime.cells = new bool[spaceSize * timeSize];
	spaceTime.spaceSize = spaceSize;
	spaceTime.timeSize = timeSize;

	memset(spaceTime.cells, 0, sizeof(bool) * spaceTime.spaceSize * spaceTime.timeSize);
}

void deallocSpaceTime(SpaceTime & spaceTime)
{
	delete spaceTime.cells;
	spaceTime.spaceSize = 0ull;
	spaceTime.timeSize = 0ull;
}

bool isSpaceTimeReady(SpaceTime const & spaceTime)
{
	return spaceTime.spaceSize > 0 && spaceTime.timeSize > 0;
}

void randomizeSpace(SpaceTime & spaceTime, size_t time, double probability)
{
	assert(time < spaceTime.timeSize);

	for (size_t s{}; s < spaceTime.spaceSize; ++s) {
		cell(spaceTime, s, time) = randomEvent(probability);
	}
}

bool& cell(SpaceTime & spaceTime, size_t space, size_t time)
{
	assert(space < spaceTime.spaceSize && time < spaceTime.timeSize);

	return spaceTime.cells[spaceTime.spaceSize * time + space];
}

bool cell(SpaceTime const & spaceTime, size_t space, size_t time)
{
	assert(space < spaceTime.spaceSize && time < spaceTime.timeSize);

	return spaceTime.cells[spaceTime.spaceSize * time + space];
}

void initializeRule(Rule & rule)
{
	rule.value = 0;
	memset(rule.table, 0, sizeof(bool) * 2 * 2 * 2);
}

void setRule(Rule & rule, rule_t ruleValue)
{
	rule.value = ruleValue;
	rule.table[0][0][0] = isBitSet(ruleValue, 0);
	rule.table[0][0][1] = isBitSet(ruleValue, 1);
	rule.table[0][1][0] = isBitSet(ruleValue, 2);
	rule.table[0][1][1] = isBitSet(ruleValue, 3);
	rule.table[1][0][0] = isBitSet(ruleValue, 4);
	rule.table[1][0][1] = isBitSet(ruleValue, 5);
	rule.table[1][1][0] = isBitSet(ruleValue, 6);
	rule.table[1][1][1] = isBitSet(ruleValue, 7);
}

bool stateFromRule(Rule const & rule, bool left, bool center, bool right)
{
	return rule.table[left][center][right];
}

void nextRule(Rule & rule, size_t increment)
{
	setRule(rule, (rule_t)((rule.value + increment) % maxRuleValueCount));
}

void evolveOverSpace(SpaceTime & spaceTime, size_t currentTime, Rule const & rule)
{
	assert(currentTime > 0 && currentTime < spaceTime.timeSize);

	cell(spaceTime, 0, currentTime) = cell(spaceTime, spaceTime.spaceSize - 1, currentTime) = false;
	for (size_t space{ 1 }; space < spaceTime.spaceSize - 1; ++space) {
	//	v-----------------------------------------------------------v--- utilisent la version non const de cell
		cell(spaceTime, space, currentTime) = stateFromRule(rule,	cell(spaceTime, space - 1, currentTime - 1),
																	cell(spaceTime, space    , currentTime - 1),
																	cell(spaceTime, space + 1, currentTime - 1));
	}
}

void evolveOverTime(SpaceTime & spaceTime, Rule const & rule)
{
	assert(isSpaceTimeReady(spaceTime));

	randomizeSpace(spaceTime);
	for (size_t time{ 1 }; time < spaceTime.timeSize; ++time) {
		evolveOverSpace(spaceTime, time, rule);
	}
}

std::string spaceTimeToString(SpaceTime const & spaceTime, char nonActiveChar, char activeChar, bool includeTimeStamp)
{
	assert(isSpaceTimeReady(spaceTime));

	std::stringstream strStream;
	for (size_t time{}; time < spaceTime.timeSize; ++time) {
		if (includeTimeStamp) {
			strStream << std::setfill('0') << std::setw(3) << time << "   ";
		}
		for (size_t space{}; space < spaceTime.spaceSize; ++space) {
			//            v--- doit utiliser la version const de cell
			strStream << (cell(spaceTime, space, time) ? activeChar : nonActiveChar);
		}
		strStream << std::endl;
	}

	return strStream.str();
}
