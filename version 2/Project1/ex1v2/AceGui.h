#ifndef ACE_GUI_H
#define ACE_GUI_H


#include "ECA.h"


// Affiche le message d'accueil
void showWelcomeScreen();
// Demande � l'usager les informations sur la taille de l'univers d�sir�e
void getUniverseSizes(size_t & spaceSize, size_t & timeSize, size_t minSpaceSize, size_t maxSpaceSize, size_t minTimeSize, size_t maxTimeSize);
// Confirme que l'usager d�sire quitter
bool isQuitting();

// Affiche le r�sultat d'une �volution
void showEvolution(SpaceTime const & spaceTime, Rule const & rule, char nonActiveChar = ' ', char activeChar = '*', bool includeTimeStamp = true);
// D�termine la prochaine action selon le choix de l'usager
bool nextAction(Rule & rule);

// Effectue la boucle principale de la simulation
void simulate(SpaceTime & spaceTime, Rule & rule, char inactiveChar, char activeChar);


#endif // ACE_GUI_H