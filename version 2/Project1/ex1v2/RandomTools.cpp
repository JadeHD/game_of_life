#include "RandomTools.h"


#include <random>


bool randomEvent(double probability)
{
	static std::random_device randomDevice;
	static std::mt19937 randomGenerator(randomDevice());
	std::bernoulli_distribution randomDistribution(probability);
	return randomDistribution(randomGenerator);
}

