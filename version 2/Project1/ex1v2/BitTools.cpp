#include "BitTools.h"


#include <sstream>
#include <cassert>


bool isBitSet(size_t number, size_t digit)
{
	assert(digit < sizeof(size_t) * 8);

	return (number >> digit) & 0b1;
}

std::string int2bin(int value, size_t nFirstDigit)
{
	assert(nFirstDigit <= sizeof(int) * 8);

	std::stringstream strStream;
	for (int i{ (int)nFirstDigit - 1 }; i >= 0; --i) {
		strStream << isBitSet(value, i) ? '1' : '0';
	}

	return strStream.str();
}


