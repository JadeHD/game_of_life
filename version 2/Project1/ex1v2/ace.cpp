// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Exercice 1 - version 2
// 
// Exercice incr�mental d'approfondissement du langage C++
// 
// Auteur : CVM
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Inclusion des librairies
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "AceGui.h"


int main()
{
	// D�finition des constantes utilis�es
	const size_t minSpaceSize{ 20 };
	const size_t maxSpaceSize{ 100 };
	const size_t minTimeSize{ 5 };
	const size_t maxTimeSize{ 250 };
	const char inactiveChar{ ' ' };
	const char activeChar{ '*' };

	// D�finition des variables utilis�es
	size_t spaceSize{}, timeSize{};
	SpaceTime spaceTime;
	initializeSpaceTime(spaceTime);
	Rule rule;
	initializeRule(rule);

	// Message d'accueil
	showWelcomeScreen();

	// D�termine la taille de l'univers
	getUniverseSizes(spaceSize, timeSize, minSpaceSize, maxSpaceSize, minTimeSize, maxTimeSize);
	allocSpaceTime(spaceTime, spaceSize, timeSize);
	
	// Effectue la boucle principale
	simulate(spaceTime, rule, inactiveChar, activeChar);
	
	// Lib�re l'univers avant de terminer le programme.
	deallocSpaceTime(spaceTime);
	return 0;
}

