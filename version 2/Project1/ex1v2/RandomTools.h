#ifndef RANDOM_TOOLS_H
#define RANDOM_TOOLS_H


// G�n�re un �v�nement al�atoire.
// Retourne vrai selon la probabilit� sp�cifi�e entre 0 et 1.
bool randomEvent(double probability = 0.5);


#endif // RANDOM_TOOLS_H