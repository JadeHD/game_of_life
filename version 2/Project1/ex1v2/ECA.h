#ifndef ECA_H
#define ECA_H


#include <string>


// Cette structure poss�de les informations li�es aux donn�es fondamentales de l'automate.
// L'objectif est de contenir toutes les donn�es d'une simulation
// cells : le pointeur vers le tableau dynamique 2d
// spaceSize : la taille de l'espace
// timeSize : la taille du temps
using SpaceTime = struct {
	bool * cells;
	size_t spaceSize;
	size_t timeSize;
};
//	Exemple avec typedef au lieu du mot cl� using utilis� depuis C++11
// ---------------------------
//	typedef struct {
//		bool * cells;
//		size_t spaceSize;
//		size_t timeSize;
//	} SpaceTime;


// La r�gle est d�finie par un tableau 3d - une dimension pour chaque cellule 
// consid�r�e lors de l'�tude du voisinage.
// Cette approche est moins performante que la pr�c�dente mais parfaitement 
// valide (pr�sent�e ici � titre p�dagogique)
size_t const maxRuleValueCount{ (size_t)pow(2, 8) }; // Ces constantes sont disponibles partout o� est r�f�renc� ce fichier!
size_t const maxRuleTableCount{ (size_t)pow(2, 3) };
using rule_t = unsigned char;
using Rule = struct {
	rule_t value;
	bool table[2][2][2];
};


// S'assure que la structure soit parfaitement initialis�e (� vide).
void initializeSpaceTime(SpaceTime & spaceTime);
// R�alise l'allocation de m�moire n�cessaire.
void allocSpaceTime(SpaceTime & spaceTime, size_t sSize, size_t tSize);
// Lib�re la m�moire pr�alablement allou�e
void deallocSpaceTime(SpaceTime & spaceTime);
// Retourne si l'univers est pr�t et poss�de des dimensions
bool isSpaceTimeReady(SpaceTime const & spaceTime);

// �tablie l'�tat de toutes les cellules al�atoirement d'un espace.
void randomizeSpace(SpaceTime & spaceTime, size_t time = 0, double probability = 0.5);

// Retourne l'acc�s en lecture et �criture � une cellule.
bool& cell(SpaceTime & spaceTime, size_t space, size_t time);		// version non const
bool cell(SpaceTime const & spaceTime, size_t space, size_t time);	// version const

// S'assure que le tableau RuleTable soit initialis�e (� false).
void initializeRule(Rule & rule);
// D�termine la r�gle
void setRule(Rule & rule, rule_t ruleValue);
// Retourne le nouvel �tat d'une cellule d'apr�s les 3 valeurs de voisinage et la r�gle en cours.
bool stateFromRule(Rule const & rule, bool left, bool center, bool right);
// Incr�mente la valeur de la r�gle de la valeur sp�cifi�e
void nextRule(Rule & rule, size_t increment = 1);

// R�alise l'�volution sur l'espace (horizontalement)
void evolveOverSpace(SpaceTime & spaceTime, size_t currentTime, Rule const & rule);
// R�alise l'�volution dans le temps (verticalement)
void evolveOverTime(SpaceTime & spaceTime, Rule const & rule);

// G�n�re une cha�ne de caract�res repr�sentant la simulation.
std::string spaceTimeToString(SpaceTime const & spaceTime, char nonActiveChar, char activeChar, bool includeTimeStamp = true);


#endif // ECA_H