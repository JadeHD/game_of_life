#ifndef BIT_TOOLS_H
#define BIT_TOOLS_H


#include <string>


// Retourne vrai si un bit est actif
bool isBitSet(size_t number, size_t digit);

// Effectue la conversion d'un nombre entier vers 
// un text représentant le nombre binaire.
std::string int2bin(int value, size_t nFirstDigit);


#endif // BIT_TOOLS_H