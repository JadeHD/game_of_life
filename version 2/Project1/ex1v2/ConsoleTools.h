#ifndef CONSOLE_TOOLS_H
#define CONSOLE_TOOLS_H


#include <string>


// Vide l'�cran
void clearScreen();

// Retourne la saisie d'un caract�re au clavier.
// Le caract�re retourn� est mis en majuscule.
char getUpperChar();

// Retourne vrai si le caract�re saisie au clavier 
// correspond au caract�re attendu (insensible � la casse).
bool validateUpperChar(char capChar);

// Retourne un entier saisie au clavier.
int getInt(std::string const & title);
// Retourne un entier born� saisie au clavier.
int getInt(std::string title, int min, int max, bool addLimitsToTitle = true);


#endif // CONSOLE_TOOLS_H