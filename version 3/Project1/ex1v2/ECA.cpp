#include "ECA.h"


#include "RandomTools.h"
#include "BitTools.h"
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <algorithm>


Cells  initializeSpaceCells(size_t spaceSize)
{
	Cells cell;
	cell.resize(spaceSize);
	return cell;
}

void allocSpaceTime(SpaceTime & spaceTime, size_t spaceSize, size_t timeSize)
{
	//assert(spaceSize >= 3 && timeSize > 0); // le 3 garantie qu'on aura un espace viable pour la suite - ainsi, pas besoin de mettre des if partout pour valider le dépassement de l'espace lors des calculs.
	/*
	if (spaceTime.cells) {
		deallocSpaceTime(spaceTime);
	}*/

	spaceTime.resize(timeSize);
	
	std::generate(spaceTime.begin(), spaceTime.end(), [s=spaceSize]() mutable { return initializeSpaceCells(s); });
	

}


void deallocSpaceTime(SpaceTime & spaceTime)
{
	delete &spaceTime;
}

bool isSpaceTimeReady(SpaceTime  spaceTime)
{
	return spaceTime.size() > 0 && spaceTime[0].size() > 0;
}

void randomizeSpace(SpaceTime & spaceTime, size_t time, double probability)
{
	//assert(time < spaceTime.timeSize);

	for (size_t s{}; s < spaceTime[0].size(); ++s) {
		spaceTime[time][s] = randomEvent(probability);
	}
}





void initializeRule(Rule & rule)
{
	rule.value = 0;
	memset(rule.table, 0, sizeof(bool) * 2 * 2 * 2);
}

void setRule(Rule & rule, rule_t ruleValue)
{
	rule.value = ruleValue;
	rule.table[0][0][0] = isBitSet(ruleValue, 0);
	rule.table[0][0][1] = isBitSet(ruleValue, 1);
	rule.table[0][1][0] = isBitSet(ruleValue, 2);
	rule.table[0][1][1] = isBitSet(ruleValue, 3);
	rule.table[1][0][0] = isBitSet(ruleValue, 4);
	rule.table[1][0][1] = isBitSet(ruleValue, 5);
	rule.table[1][1][0] = isBitSet(ruleValue, 6);
	rule.table[1][1][1] = isBitSet(ruleValue, 7);
}

bool stateFromRule(Rule const & rule, bool left, bool center, bool right)
{
	return rule.table[left][center][right];
}

void nextRule(Rule & rule, size_t increment)
{
	setRule(rule, (rule_t)((rule.value + increment) % maxRuleValueCount));
}

void evolveOverSpace(SpaceTime & spaceTime, size_t currentTime, Rule const & rule)
{
	//assert(currentTime > 0 && currentTime < spaceTime.timeSize);


	spaceTime[currentTime][0]  = stateFromRule(rule, spaceTime[currentTime - 1][spaceTime[0].size() - 1],
														spaceTime[currentTime - 1][0],
														spaceTime[currentTime - 1][ 1]);
	for (size_t space{ 1 }; space < spaceTime[0].size() - 1; ++space) {
	//	v-----------------------------------------------------------v--- utilisent la version non const de cell
		spaceTime[currentTime][space] = stateFromRule(rule, spaceTime[currentTime-1][space-1],
																	spaceTime[currentTime-1][space],
																		spaceTime[currentTime-1][space+1]);
	}
	spaceTime[currentTime][spaceTime[0].size() - 1] = stateFromRule(rule, spaceTime[currentTime - 1][spaceTime[0].size() - 2],
																	spaceTime[currentTime - 1][spaceTime[0].size() - 1],
																		spaceTime[currentTime - 1][0]);
}

void evolveOverTime(SpaceTime & spaceTime, Rule const & rule)
{
	//assert(isSpaceTimeReady(spaceTime));

	randomizeSpace(spaceTime);
	for (size_t time{ 1 }; time < spaceTime.size(); ++time) {
		evolveOverSpace(spaceTime, time, rule);
	}
}

std::string spaceTimeToString(SpaceTime const & spaceTime, char nonActiveChar, char activeChar, bool includeTimeStamp)
{
	//assert(isSpaceTimeReady(spaceTime));

	std::stringstream strStream;
	for (size_t time{}; time < spaceTime.size(); ++time) {
		if (includeTimeStamp) {
			strStream << std::setfill('0') << std::setw(3) << time << "   ";
		}
		for (size_t space{}; space < spaceTime[0].size(); ++space) {
			//            v--- doit utiliser la version const de cell
			strStream << (spaceTime[time][space] ? activeChar : nonActiveChar);
		}
		strStream << std::endl;
	}

	return strStream.str();
}
