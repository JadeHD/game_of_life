#include "AceGui.h"


#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "ConsoleTools.h"
#include "BitTools.h"
#include <stdexcept>
#include <cmath>

// D�finition des constantes utilis�es

// D�finition des cha�nes de caract�res utilis�es
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const std::string welcomeScreenText(
R"-(


ACE
Logiciel de simulation d'un automate cellulaire elementaire



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   Actions disponibles pendant le deroulement du programme :
      (Q)uitter
      (R)eprendre la derniere regle
      Poursuivre avec la prochaine regle (toute autre touche)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 



appuyer sur une touche ...)-");
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const std::string questionToQuit(
R"-(
Desirez-vous quitter?
   (O)ui
   ... toute autre touche pour poursuivre)-");
const char quitConfirmationCharacter{ 'O' };
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const char confirmationQuittingChar{ 'O' };
const std::string askSpaceSize(std::string("Veuillez entrer la taille de l'espace"));
const std::string askTimeSize(std::string("Veuillez entrer la taille du temps"));


// Fonction utilitaire locale (simplement un exemple p�dagogique)
std::string lineTitle(SpaceTime const & spaceTime, size_t offset = 0)
{
	return std::string(spaceTime[0].size() + offset, '-');
}


void showWelcomeScreen()
{
	std::cout << welcomeScreenText;
	getUpperChar();
}

bool isQuitting()
{
	clearScreen();
	std::cout << questionToQuit;
	return validateUpperChar(confirmationQuittingChar);
}

void showNoColumns(size_t space)
{
	std::cout << "      ";
	size_t dec = floor(space / 10) +1;
	size_t unit = space - ((dec - 1) * 10);

	for (int j{}; j < dec * 10; ++j)
	{
		if (j == space)
		{
			break;
		}
		std::cout << floor(j/10);
	}
	std::cout << std::endl;

	std::cout << "      ";
	for (int i{}; i < dec; ++i) 
	{
		for (int j{}; j < 10; ++j)
		{
			if (i == dec - 1 && j == unit)
			{
				break;
			}
			std::cout << j;
		}
	}
	
	std::cout << std::endl;
}

void showEvolution(SpaceTime const & spaceTime, Rule const & rule, char nonActiveChar, char activeChar, bool includeTimeStamp)
{
	static std::string leftSep("   [");
	static std::string rightSep("]");

	clearScreen();
	std::string line(lineTitle(spaceTime, leftSep.length() + 1));
	std::cout	<< line << std::endl
				<< "Current rule : " << std::setfill('0') << std::setw(3) << (int)rule.value << leftSep << int2bin(rule.value, 8) << rightSep << std::endl
				<< line << std::endl
				<< std::endl;
	
	showNoColumns(spaceTime[0].size());

	std::cout		<< spaceTimeToString(spaceTime, nonActiveChar, activeChar, includeTimeStamp) << std::endl
				<< line << std::endl;
}

bool nextAction(Rule & rule)
{
	switch (getUpperChar()) {
		case 'Q':
			return isQuitting();
		case 'R':
			break;
		default:
			nextRule(rule);
	}

	return false;
}

void simulate(SpaceTime & spaceTime, Rule & rule, char inactiveChar, char activeChar)
{
	//assert(isSpaceTimeReady(spaceTime));



	randomizeSpace(spaceTime);

	// Effectue la boucle principale
	bool quitting{};
	while (!quitting) {
		evolveOverTime(spaceTime, rule);
		showEvolution(spaceTime, rule, inactiveChar, activeChar, true);

		quitting = nextAction(rule);
	}
}

void getUniverseSizes(size_t & spaceSize, size_t & timeSize, size_t minSpaceSize, size_t maxSpaceSize, size_t minTimeSize, size_t maxTimeSize)
{
	clearScreen();
	spaceSize = (size_t)getInt(askSpaceSize, minSpaceSize, maxSpaceSize, true);
	timeSize = (size_t)getInt(askTimeSize, minTimeSize, maxTimeSize, true);
}

