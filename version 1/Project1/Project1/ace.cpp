// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Exercice 1 - version 1
// 
// Exercice incr�mental d'approfondissement du langage C++
// 
// Auteur : CVM
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Note : 
//	- Afin d'all�ger ce premier exemple, les fonctions r�alis�es g�rent les cas 
//		probl�matiques sans gestion externe. La strat�gie est simpliste : 
//			- chaque fonction valide tous les param�tres d'entr�e :
//				- si tous les param�tres sont valides : r�alise la t�che 
//				- si un param�tre est invalide : la fonction termine sans r�aliser sa t�che
//		Le probl�me avec cette approche est que le programme continue sans autre 
//		mesure. Cette approche na�ve (et incorrecte) est suffisante pour ce 
//		premier exemple. 
//	- Cet exemple n'utilise pas les r�f�rences du C++ � titre de d�monstration 
//		p�dagogique. Sachez que leur usage est souhaitable lorque possible.
//	- Cet exemple en est un parmi plusieurs possibles. Il reste simple et 
//		pr�sente plusieurs concepts importants. Plusieurs am�liorations sont 
//		possibles et certains exemples sont donn�s dans les exercices suivants.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Inclusion des librairies
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// D�finitions
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// D�finition des constantes globales (via des variables symboliques)
const size_t spaceSize{ 80 };							// Taille de l'espace 
const size_t evolutionCount{ 24 };						// Nombre d'�volutions
const size_t timeSize{ evolutionCount + 1 };			// Taille du temps
const size_t ruleSize{ 8 };								// Taille d'une r�gle
const size_t ruleCount{ (size_t)pow(2, ruleSize) };		// Nombre de r�gles possibles
const char stateActive{ '*' };							// Caract�re repr�sentant un �tat actif
const char stateInactive{ ' ' };						// Caract�re repr�sentant un �tat inactif
// D�finition des cha�nes de caract�res utilis�es
const char mainTitle[]{ "ACE" };
const char subTitle[]{ "Logiciel de simulation d'un automate cellulaire elementaire" };
const char lineTitle[]{ "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" };
const char menu[]{ "Menu :\n\t(Q)uitter\n\t(R)eprendre la regle\n\t(...) toute autre touche pour poursuivre\n\n\nAppuyez sur une touche pour poursuivre..." };
const char questionToQuit[]{ "Voulez-vous quitter?\n\n\t(O)ui\n\t(...) toute autre touche pour poursuivre" };

// D�finition des types
typedef bool Space[spaceSize];							// Type repr�sentant l'espace
typedef Space SpaceTime[timeSize];						// Type repr�sentant l'espace dans le temps
typedef bool Rule[ruleSize];							// Type repr�sentant une r�gle

// D�finition des fonctions
// Fonctions utilitaires d'int�raction avec la console
void clearScreen();										// Efface la console
char getUpperChar();									// Fonction retournant la majuscule d'un caract�re lu � la console
bool isQuitting();										// Retourne si l'usager confirme son d�sire de quitter l'application
void showWelcomeScreen();								// Affiche la page d'accueil
void showSpaceTime(SpaceTime spaceTime, bool showTime);	// Affiche l'image cr��e par une simulation
void showTic(SpaceTime spaceTime, size_t ruleValue);	// Affiche la r�gle courante et l'image cr��e par la simulation
bool getTicInputUser(size_t * rule);					// Demande � l'usager quelle instruction faire 
// Fonctions utilitaires
void int2bin(int value, size_t nFirstDigit, char string[], size_t stringSize);	// Effectue la conversion d'un nombre entier en nombre binaire dans une cha�ne de caract�res
bool randomEvent();										// G�n�re un �v�nement al�atoire (50%/50% - vrai/faux)
// Fonctions de traitement de l'automate
void initializeSpaceTime(SpaceTime spaceTime);			// Initialise toute la simulation � false
void randomizeState(bool * cell);						// D�termine al�atoirement l'�tat d'une cellule
void randomizeSpace(Space space);						// Initialise l'espace al�atoirement
bool tic(SpaceTime spaceTime, size_t * rule);			// Effectue un pas de simulation
int getNeighborhoodValue(SpaceTime spaceTime, size_t currentTime, size_t currentSpace); // Retourne la valeur du voisinage de la cellule � traiter (8 valeurs possibles entre 000 et 111)
void evolveOverSpace(SpaceTime spaceTime, Rule rule, size_t currentTime);	// Effectue l'�volution pour un temps donn�
void evolveOverTime(SpaceTime spaceTime, Rule rule);		// Effectue l'�volution pour toute la simulation
void ticSpaceTime(SpaceTime spaceTime, size_t ruleValue);	// Effectue l'assignation de la r�gle et effectue l'�volution


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Point d'entr�e du programme
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int main()
{
	// Initialisation du g�n�rateur de nombre al�atoire
	srand(unsigned int(time(nullptr)));

	// Conteneur de la simulation et initialisation
	size_t currentRule{};
	SpaceTime spaceTime;
	initializeSpaceTime(spaceTime);
	
	// Message d'accueil
	showWelcomeScreen();

	// Boucle de traitement
	while (tic(spaceTime, &currentRule));

	return 0;
}

void clearScreen()
{
	system("cls");
}

char getUpperChar()
{
	int keyCode{ _getch() };				// _getch must be call twice to catch the key code
	int functionKey{ _getch() };			// and the function key
	return (char)toupper(keyCode);
}

void showWelcomeScreen()
{
	printf("\n\n\n%s\n%s\n\n\n%s\n", mainTitle, subTitle, menu);
	getUpperChar();
}

void showSpaceTime(SpaceTime spaceTime, bool showTime)
{
	if (spaceTime) {
		for (size_t time{}; time < timeSize; ++time) {
			printf("%02zd   ", time);
			for (size_t space{}; space < spaceSize; ++space) {
				printf("%c", spaceTime[time][space] ? stateActive : stateInactive);
			}
			printf("\n");
		}
	}
}

void showTic(SpaceTime spaceTime, size_t ruleValue)
{
	char ruleBinValue[256];
	int2bin((int)ruleValue, 8, ruleBinValue, 256);

	clearScreen();
	printf("%s\n", lineTitle);
	printf("Current rule : %zd (%s)\n", ruleValue, ruleBinValue);
	printf("%s\n", lineTitle);
	showSpaceTime(spaceTime, true);
}

bool isQuitting()
{
	clearScreen();
	printf("%s", questionToQuit);

	return getUpperChar() == 'O';
}

bool getTicInputUser(size_t * rule)
{
	if (!rule) {
		return true;
	}

	bool quitting{ false };
	switch (getUpperChar()) {
		case 'Q':
			quitting = isQuitting();
		case 'R':
			break;
		default:
			*rule = (*rule + 1) % ruleCount;
	}

	return !quitting;
}

bool tic(SpaceTime spaceTime, size_t * rule)
{
	if (!rule) {
		return false;
	}

	ticSpaceTime(spaceTime, *rule);
	showTic(spaceTime, *rule);
	return getTicInputUser(rule);
}

void int2bin(int value, size_t nFirstDigit, char string[], size_t stringSize)
{
	if (nFirstDigit <= sizeof(int) * 8 && string && nFirstDigit < stringSize) {
		for (size_t i{}; i < nFirstDigit; ++i) {
			string[nFirstDigit - 1 - i] = ((value >> i) & 1 ? '1' : '0');
		}
		string[nFirstDigit] = '\0';
	}
}

bool randomEvent()
{
	return rand() < RAND_MAX / 2;
}

void randomizeState(bool * cell)
{
	if (cell) {
		*cell = randomEvent();
	}
}

void initializeSpaceTime(SpaceTime spaceTime)
{
	if (spaceTime) {
		memset(spaceTime, 0, sizeof(bool) * spaceSize * timeSize);
	}
}

void randomizeSpace(Space space)
{
	if (space) {
		for (size_t iSpace{}; iSpace < spaceSize; ++iSpace) {
			randomizeState(space + iSpace);
		}
	}
}

void setRule(Rule rule, size_t ruleValue)
{
	if (rule && ruleValue < ruleCount) {
		for (size_t r{}; r < ruleSize; ++r) {
			rule[r] = (ruleValue >> r) & 1;
		}
	}
}

// Cette fonction ne fait aucune validation des param�tres d'entr�e.
// C'est la responsabilit� de la fonction appelante de faire cette validation.
// Pr�conditions :	spaceTime must be valid
//					currentTime	 E [0, tSize[
//					currentSpace E [1, tSize - 1[
int getNeighborhoodValue(SpaceTime spaceTime, size_t currentTime, size_t currentSpace)
{
	return	(int(spaceTime[currentTime][currentSpace + 1]) << 0) |
			(int(spaceTime[currentTime][currentSpace    ]) << 1) |
			(int(spaceTime[currentTime][currentSpace - 1]) << 2);
}

void evolveOverSpace(SpaceTime spaceTime, Rule rule, size_t currentTime)
{
	if (spaceTime && currentTime > 0 && currentTime < timeSize && rule) {
		spaceTime[currentTime][0] = spaceTime[currentTime][spaceSize - 1] = false;
		for (size_t space{ 1 }; space < spaceSize - 1; ++space) {
			spaceTime[currentTime][space] = rule[getNeighborhoodValue(spaceTime, currentTime - 1, space)];
		}
	}
}

void evolveOverTime(SpaceTime spaceTime, Rule rule)
{
	if (spaceTime && rule) {
		randomizeSpace(spaceTime[0]);
		for (size_t time{ 1 }; time < timeSize; ++time) {
			evolveOverSpace(spaceTime, rule, time);
		}
	}
}

void ticSpaceTime(SpaceTime spaceTime, size_t ruleValue)
{
	if (spaceTime && ruleValue < ruleCount) {
		Rule rule;
		setRule(rule, ruleValue);
		evolveOverTime(spaceTime, rule);
	}
}

